import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.PWM as PWM
import Adafruit_BBIO.ADC as ADC
import time
from time import sleep


# Digital input reading
class DigitalIn():
        def __init__(self, pin):
                self.pin = pin
                GPIO.setup(pin, GPIO.IN)

        def read(self):
                return GPIO.input(self.pin)

# Digital output writing
class DigitalOut():
        def __init__(self, pin):
                self.pin = pin
                GPIO.setup(pin, GPIO.OUT)
                
        def set(self, value):
                GPIO.output(self.pin, value)

# Digital output pwm
class DigitalPWM():
        def __init__(self, pin, freq=2000, pol=0):
                self.pin = pin
                self.freq = freq
                self.pol = pol
                PWM.start(self.pin, 0, self.freq)
        

        def set(self, duty):
                
                PWM.set_duty_cycle(self.pin, duty)

        def stop(self):
                PWM.stop(self.pin)

#Analog input reading
class AnalogIn():
        def __init__(self, pin):
                self.pin = pin

        def read(self):
                ADC.read(self.pin) # Read twice due to some driver bug.
                return ADC.read(self.pin)

ADC.setup()

class AnalogPot(AnalogIn):
        def __init__(self, pin, minL, min, max, maxL):
                AnalogIn.__init__(self, pin)
                self.minL = minL
                self.min = min
                self.max = max
                self.maxL = maxL

        
        

        def read_norm(self):
                pot_val = self.read()

               # print(pot_val)

                if (pot_val <= self.minL) or (self.maxL <= pot_val):
                        return -1
                elif (self.minL < pot_val) and (pot_val < self.min):
                        return 0
                elif (self.max < pot_val) and (pot_val < self.maxL):
                        return 1
                else:
                        mod_val = pot_val - self.min
                        return mod_val / (self.max - self.min)


pin_list = []

# Pedal Box

throt_pot_1 = AnalogPot('P9_39', 0.03, 0.07, 0.38, 0.5)
throt_pot_2 = AnalogPot('P9_37', 0.12, 0.17, 0.46, 0.6)
brake = AnalogPot('P9_35', 0.05, 0.1, 0.9, 0.95)


# Dashboard
brake_regen = AnalogIn('P9_38')
ignit_btn = AnalogIn('P9_33')
perf_switch = AnalogIn('P8_10')
#ignit_btn = DigitalIn('P8_12')
#perf_switch = DigitalIn('P8_10')
fault_led = DigitalOut('P8_8')
speed_sig = DigitalOut('P8_19')
fuel_sig = DigitalOut('P8_13')


# Accumulator
neg_air = DigitalIn('P8_18')
state_charge = AnalogIn('P9_40')
tsms = DigitalIn('P8_16')
# limit_low = DigitalIn('P8_18')


# Buzzer
hv_buzz = DigitalOut('P8_46')




# Bamocar Manual
man_bmc_frg = DigitalOut('P9_23')
man_bmc_rfe = DigitalOut('P9_25')
man_throt_left = DigitalPWM('P9_21')
man_throt_right = DigitalPWM('P9_22')


"""
Useful Functions related to IO
"""


GPIO.setup("P8_12", GPIO.IN)
GPIO.setup("P8_44", GPIO.OUT)




man_bmc_rfe.set(GPIO.LOW) #rfe can never be set 
man_bmc_frg.set(GPIO.LOW) 



drifting_enabled = False
ignit_btn_threshold = .12
car_on = False
print("RUNNING")
while True:
   #  print(brake.read())
   # print(ignit_btn.read())
   # print(ignit_btn.read())
    if ignit_btn.read() > ignit_btn_threshold: 
        print("BUTTON PRESSED") 
        car_on = not(car_on)
        man_bmc_rfe.set(GPIO.HIGH) #rfe can never be set, setting just in case
        man_bmc_frg.set(GPIO.HIGH) #setting frg 
        time.sleep(4)
        print("IS CAR ON:", car_on)

           
    if car_on:  
         throt_1 = throt_pot_1.read_norm()
         throt_2 = throt_pot_2.read_norm()
         avg = float((throt_1+throt_2)/2)
       #  print(avg)
         avg *= 100
         
         if avg > 100 or throt_1 <= 0 or throt_2 <= 0:
             avg = 0

         if drifting_enabled == False and brake.read() == 1:
             print("BREAKING OVERRIDE") 
             avg = 0

        # print(avg) 
         man_throt_left.set(avg)
         man_throt_right.set(avg)

 

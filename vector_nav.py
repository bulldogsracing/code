#!/usr/bin/python

"""
This program should read from the VectorNAV using a serial port

"""

import serial
import time


DEVICE = "/dev/ttyS4"
ser = serial.Serial(port = DEVICE, baudrate=115200, timeout=1)
#ser.write("$VNWRG,06,0*6C\r\n".encode())   #turns off all async 
#ser.write("$VNWRG,06,28*XX\r\n".encode())  #sets async. register
#ser.write("$VNWNV*57\r\n".encode())  #write to non-volatile memory
#print(str(ser.read(100)))
#for i in range(0, 10):
#    print(str(ser.read(100)))

#ser.write("$VNWRG,75,0,16,01,0029*XX\r\n".encode())
#print(str(ser.read(100)))
#ser.write("$VNWRG,76,0,16,01,0029*XX\r\n".encode())
#print(str(ser.read(100)))
#ser.write("$VNWRG,77,0,16,01,0029*XX\r\n".encode())
#print(str(ser.read(100)))

while True:
#    ser.write('$VNRRG,18*XX\r\n'.encode())
	x = str(ser.read(100))
#    print("x:",x)
	x = x.split(",")
	
	for i in range(0, len(x)):
		elem = x[i]
		if "$VNISL" in elem:
			print("READING",x[i:i+16])
			time.sleep(1)
			#acc = x[i:i+20]
			#if len(acc) == 3:
			 #   print("acc", acc)
			  #  time.sleep(1)
#    time.sleep(1)

#print("MESSAGE:\n", x)